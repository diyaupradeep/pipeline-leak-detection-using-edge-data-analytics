#!/usr/bin/env python
# coding: utf-8

# # Leak detection
# 
# This notebook will train a model

# # Imports & Environment Setup

# In[83]:


# Define paths to model files
import os
MODELS_DIR = 'leak_models/'
if not os.path.exists(MODELS_DIR):
    os.mkdir(MODELS_DIR)
MODEL_TF = MODELS_DIR + 'leak_model'
MODEL_NO_QUANT_TFLITE = MODELS_DIR + 'leak_model_no_quant.tflite'
MODEL_TFLITE = MODELS_DIR + 'leak_model.tflite'
MODEL_TFLITE_MICRO = MODELS_DIR + 'leak_model.cc'
MODEL_TFLITE_MICRO = MODELS_DIR + 'leak_model.h'


# In[84]:


import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math

# Set seed for experiment reproducibility
seed = 42
np.random.seed(seed)
tf.random.set_seed(seed)


# In[85]:


tf.__version__


# # Data Preparation

# In[86]:



#columns don't come named, so these are the names for use
#column_names = ['P1', 'P2', 'P1-P2', '(P1-P2)', 'LABEL']

#turn it into a Pandas Dataset
dataset = pd.read_csv('/home/diya/Desktop/total_data.csv')


# In[87]:


#drop nulls
dataset = dataset.dropna()

#train / test split
train_dataset = dataset.sample(frac=0.8, random_state=seed)
test_dataset = dataset.drop(train_dataset.index)

train_features = train_dataset.copy()
test_features = test_dataset.copy()

#label is our target
train_labels = train_features.pop('LABEL')
test_labels = test_features.pop('LABEL')

#select only the sensorvalue column
train_features = np.array(train_features['(P1-P2)'])
test_features = np.array(test_features['(P1-P2)'])


# # Model Training

# In[88]:


model = tf.keras.Sequential()

#only takes one feature
model.add(keras.layers.Dense(16, activation='relu', input_shape=(1,)))
model.add(keras.layers.Dense(16, activation='relu'))
#only one output
model.add(keras.layers.Dense(1))

model.compile(optimizer='adam', loss="mse", metrics=["mae"])


# In[89]:


# Train the model
history = model.fit(train_features, train_labels, epochs=400, batch_size=64,
                    validation_data=(test_features, test_labels))

# Save the model to disk
model.save(MODEL_TF)


# # Model Evaluation

# In[90]:


#we take one input and provide one prediction
model.predict([50])


# In[91]:


#plotting loss over our epochs
train_loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(1, len(train_loss) + 1)

plt.figure(figsize=(10, 4))


plt.plot(epochs, train_loss, 'g.', label='Training loss')
plt.plot(epochs, val_loss, 'b.', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')


# In[92]:


# Calculate and print the loss on our test dataset
test_loss, test_mae = model.evaluate(test_features, test_labels)

# Make predictions based on our test dataset
y_test_pred = model.predict(test_features)

# Graph the predictions against the actual values
plt.clf()
plt.title('Comparison of predictions and actual values')
plt.plot(test_features, test_labels, 'b.', label='Actual values')
plt.plot(test_features, y_test_pred, 'r.', label='TF predicted')
plt.legend()
plt.show()


# # Converting Models
# A representative dataset is provided and two models saved, one in TFLite format without quantization and one in TFLite format with int8 quantization for activations and weights.

# In[93]:


#preparing features for evaluating models
x_test = tf.convert_to_tensor(train_features,dtype=tf.float32)


def representative_dataset():
  for i in range(300):
    yield([tf.reshape(x_test[i],(1, 1))])


# In[94]:


# Convert the model to TFLite without quantization
converter = tf.lite.TFLiteConverter.from_saved_model(MODEL_TF)
model_no_quant_tflite = converter.convert()
open(MODEL_NO_QUANT_TFLITE, "wb").write(model_no_quant_tflite)


# Convert and save the model with quantization
converter.optimizations = [tf.lite.Optimize.DEFAULT]
# Enforce integer only quantization
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
converter.inference_input_type = tf.int8
converter.inference_output_type = tf.int8
# Create and provide a representative dataset
converter.representative_dataset = representative_dataset
model_tflite = converter.convert()
open(MODEL_TFLITE, "wb").write(model_tflite)


# ## 2. Compare Model Performance
# Compare loss or performance on the test set to evaluate how much fidelity is lost with compression and quantization.

# In[95]:


x_test = test_features.copy()
x_test = x_test.reshape((x_test.size, 1))
x_test = x_test.astype(np.float32)


# In[96]:


#first, make predictions using the model we trained, no TFLite or quant
y_test_pred_tf = model.predict(x_test)


# In[97]:


#next, TFLite with no quant

#initialize and allocate memory
no_q_interpreter = tf.lite.Interpreter(model_content=model_no_quant_tflite)
no_q_interpreter.allocate_tensors()

y_test_pred_no_quant_tflite = np.empty(x_test.size, dtype=np.float32)
for i in range(len(x_test)):
    no_q_interpreter.set_tensor(no_q_interpreter.get_input_details()[0]["index"], [x_test[i]])
    no_q_interpreter.invoke()
    y_test_pred_no_quant_tflite[i] = no_q_interpreter.get_tensor(no_q_interpreter.get_output_details()[0]["index"])[0]


# ### Prediction with Quantization
# It's a bit more involved to get these predictions, we must use the scaling criteria in our interpreter

# In[98]:


q_interpreter = tf.lite.Interpreter(model_content=model_tflite)
q_interpreter.allocate_tensors()


# In[99]:


#interpreter gives us information we'll need to prepare input and output data
q_interpreter.get_input_details()


# In[100]:


input_details = q_interpreter.get_input_details()[0]
output_details = q_interpreter.get_output_details()[0]

input_scale, input_zero_point = input_details["quantization"]

x_test_ = x_test / input_scale + input_zero_point
x_test_ = x_test_.astype(input_details["dtype"])
  
# Invoke the interpreter
y_test_pred_tflite = np.empty(x_test_.size, dtype=output_details["dtype"])
for i in range(len(x_test_)):
    q_interpreter.set_tensor(input_details["index"], [x_test_[i]])
    q_interpreter.invoke()
    y_test_pred_tflite[i] = q_interpreter.get_tensor(output_details["index"])[0]

# If required, dequantized the output layer (from integer to float)
output_scale, output_zero_point = output_details["quantization"]
y_test_pred_tflite = y_test_pred_tflite.astype(np.float32)
y_test_pred_tflite = (y_test_pred_tflite - output_zero_point) * output_scale


# In[101]:


q_interpreter.get_output_details()


# **1. Predictions**

# In[102]:


x_test = test_features
y_test = test_labels
# Compare predictions
plt.clf()
plt.title('Comparison of various models against actual values')
plt.plot(x_test, y_test, 'bo', label='Actual values')
plt.plot(x_test, y_test_pred_tf, 'ro', label='TF predictions')
plt.plot(x_test, y_test_pred_no_quant_tflite, 'bx', label='TFLite predictions')
plt.plot(x_test, y_test_pred_tflite, 'gx', label='TFLite quantized predictions')
plt.legend()
plt.show()


# **2. Loss (MSE/Mean Squared Error)**

# In[103]:


model.loss


# In[104]:


loss_function = tf.keras.losses.get(model.loss)
#have to reshape this to be wide or a long list, rather than narrow or list of lists
loss_tf = loss_function(y_test, y_test_pred_tf.reshape(1,-1)[0]).numpy()
loss_no_quant_tflite = loss_function(y_test, np.float64(y_test_pred_no_quant_tflite)).numpy()
loss_tflite = loss_function(y_test, y_test_pred_tflite).numpy()


# In[105]:


# Compare loss
df = pd.DataFrame.from_records(
    [["TensorFlow", loss_tf],
     ["TensorFlow Lite", loss_no_quant_tflite],
     ["TensorFlow Lite Quantized", loss_tflite]],
     columns = ["Model", "Loss/MSE"], index="Model").round(4)
df


# **3. Size**

# In[106]:


# Calculate size
size_tf = os.path.getsize(MODEL_TF)
size_no_quant_tflite = os.path.getsize(MODEL_NO_QUANT_TFLITE)
size_tflite = os.path.getsize(MODEL_TFLITE)


# In[107]:


#Lite for Microco# Compare size
pd.DataFrame.from_records(
    [["TensorFlow", f"{size_tf} bytes", ""],
     ["TensorFlow Lite", f"{size_no_quant_tflite} bytes ", f"(reduced by {size_tf - size_no_quant_tflite} bytes)"],
     ["TensorFlow Lite Quantized", f"{size_tflite} bytes", f"(reduced by {size_no_quant_tflite - size_tflite} bytes)"]],
     columns = ["Model", "Size", ""], index="Model")


# **Summary**

# ## Generate a TensorFlow Lite for Microcontrollers Model
# Convert the TensorFlow Lite quantized model into a C source file that can be loaded by TensorFlow Lite for Microcontrollers.

# In[108]:


# Convert to a C source file, i.e, a TensorFlow Lite for Microcontrollers model
get_ipython().system('xxd -i {MODEL_TFLITE} > {MODEL_TFLITE_MICRO}')


# In[109]:


# Print the C source file
get_ipython().system('cat {MODEL_TFLITE_MICRO}')


# In[ ]:





# In[ ]:




