
const float  OffSet = 0.483 ;

float V1, P1,V2,P2;

void setup()
{
  Serial.begin(9600);        // open serial port, set the baud rate to 9600 bps
}
void loop()
{
  //Connect sensor to Analog 0
  V1 = analogRead(0) * 5.00 / 1024;     //Sensor output voltage
  P1 = (V1 - OffSet) * 250;             //Calculate water pressure
  V2 = analogRead(1) * 5.00 / 1024;     //Sensor output voltage
  P2 = (V2 - OffSet) * 250;             //Calculate water pressure


//  Serial.print("Voltage:");
//  Serial.print(V1, 3);
//  Serial.println("V");

  //Serial.println(" Pressure at node 1:");
  Serial.print(P1, 1);
  Serial.print(",");
  Serial.print(P2, 1);
    delay(1000);
  Serial.print('\n');
 // Serial.println("ok");

    //Serial.println(",");
  //Serial.println(" KPa");
  //Serial.println(" Pressure at node 2:");
 // Serial.println(P2, 1);
 //Serial.println('/n');
  delay(100);
}
